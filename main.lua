GameState = require "libs.hump.gamestate"

gamestates = {}
debug = false

greyscaleShader = love.graphics.newShader("greyscale.glsl")

gamestates.game = require "gamestates.game"
gamestates.win = require "gamestates.win"

font = love.graphics.newFont("Comfortaa-Regular.ttf", CELL_SIZE)
fontSmall = love.graphics.newFont("Comfortaa-Regular.ttf", CELL_SIZE * 0.6)

music = love.audio.newSource("data/music.ogg", "stream")

soundeffects = {
    keypress = love.audio.newSource("data/key-press.wav", 'static'),
    nextGeneration = love.audio.newSource("data/next-generation.wav", 'static'),
    scrollDown = love.audio.newSource("data/scroll-down.wav", 'static'),
    placeCell = love.audio.newSource("data/place-cell.wav", 'static'),
    cannotPlaceCell = love.audio.newSource("data/cannot-place-cell.wav", 'static'),
    scrollUp = love.audio.newSource("data/scroll-up.wav", 'static'),
}

whiteBackground = love.graphics.newImage("data/white-background.png")

gamestates.help = require "gamestates.help"

function printWithCentre(text, centreX, centreY, f)
    f = f or font
    local width = f:getWidth(text)
    local height = f:getHeight()

    music:setLooping(true)

    love.graphics.setFont(f)
    love.graphics.print(text, centreX - width / 2, centreY - height / 2)
end


function love.load(args)
    whiteBackground:setWrap("repeat", "repeat")

    for k, v in pairs(args) do
        if v == "--leveleditor" then
            debug = true
        end
    end

    GameState.registerEvents()
    GameState.switch(gamestates.game)

    music:play()
end

extern number time;

#ifdef PIXEL
#define TIME_CONSTANT 0.5
#define DISTANCE_CONSTANT 1
#define SIN_CONSTANT 5

vec4 effect(vec4 colour, Image texture, vec2 tc, vec2 sc) {
    number v = max(cos(time * TIME_CONSTANT + (-tc.x) * DISTANCE_CONSTANT) - 0.99, 0) * SIN_CONSTANT;

    return mix(Texel(texture, tc), vec4(1,1,1,1), v) * Texel(texture, tc).a;
}
#endif // PIXEL

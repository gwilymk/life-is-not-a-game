CELLS_HORIZONTAL = 30
CELLS_VERTICAL = 20

CELL_SIZE = math.min(32, love.graphics.getWidth() / (CELLS_HORIZONTAL + 2), (love.graphics.getHeight() - 50) / (CELLS_VERTICAL+1))

BOARD_X = (love.graphics.getWidth() / 2) - CELL_SIZE * CELLS_HORIZONTAL / 2
BOARD_Y = love.graphics.getHeight() - CELL_SIZE * (CELLS_VERTICAL + 1)

local MAXIMUM_TILES_PER_TURN = 5

local SIMULATION_STEP = 0.5
local ANIMATE_LENGTH = SIMULATION_STEP / 5

local PLAYER_COLOUR = {225, 165, 0}
local NEW_PLAYER_CELL_COLOUR = {0xff, 0x91, 0x27}
local OPPONENT_COLOUR = {0, 0, 255}
local PLACEABLE_COLOUR = {50, 0, 50}

local backgroundImage = love.graphics.newImage("data/background.png")

local Cell = require "cell"
math.random = love.math.random
local lume = require "libs.lume"
local Timer = require "libs.hump.timer"

local level = require "level"
local stableShapes = require "stableshapes"

if type(level) ~= "table" then level = {} end

local state = {
    paused = "paused",
    simulating = "simulate"
}

local game = {}

-- Cell states:
-- 0 = dead
-- 1 = player
-- 2 = opponent

function game:enter(previous)
    love.math.setRandomSeed(os.time())

    self.cells = {}

    self.totalPlayer = 0
    self.totalOpponent = 0

    self.totalPlacedThisTurn = 0

    self.boardCanvas = love.graphics.newCanvas(CELL_SIZE * CELLS_HORIZONTAL, CELL_SIZE * CELLS_VERTICAL)
    self.boardShader = self.boardShader or love.graphics.newShader("board.glsl")

    self.timer = Timer.new()
    self.magicNumber = 10

    self:forEachCell(function(i, j)
        local index = i + (j - 1) * CELLS_HORIZONTAL
        self.cells[index] = Cell(level[index] or 0, self.timer)
        if level[index] == 1 then
            self.totalPlayer = self.totalPlayer + 1
        elseif level[index] == 2 then
            self.totalOpponent = self.totalOpponent + 1
        end
    end)

    self.generation = 0
    self.currentState = state.paused
    self.health = 500

    for i = 1,4 do
        self:addStableShape()
    end

    self:simulateStep(true)
end

function game:addStableShape()
    local placed = false
    local tries = 0

    -- we won't overwrite the placeable locations until we've tried 10 times.
    -- after 40 tries, start overwriting anything

    while not placed do
        local shape = lume.weightedchoice(stableShapes)
        tries = tries + 1

        local w, h = shape.width, shape.height

        -- find a place to put it (try 10 times)
        for i = 1,10 do
            local x, y = love.math.random(CELLS_HORIZONTAL), love.math.random(CELLS_VERTICAL)

            -- see if this is a blank space
            local goodSpot = true
            for a = -2,w+2 do
                for b = -2,h+2 do
                    cell = self:getCell(x + a, y + b)
                    if cell.state ~= 0 or (cell.placeable == true and tries < 10) then
                        goodSpot = tries > 40
                    end
                end
            end

            if goodSpot then
                for a = 1,w do
                    for b = 1,h do
                        local cell = self:getCell(x + a, y + b)
                        cell:setNextState(2 * shape.data[a + (b - 1) * w])
                        cell.size = 0
                    end
                end

                placed = true
                break
            end
        end
    end
end

function game:getCell(x, y)
    local cellX = (x - 1) % CELLS_HORIZONTAL + 1
    local cellY = (y - 1) % CELLS_VERTICAL + 1
    return self.cells[(cellY - 1) * CELLS_HORIZONTAL + cellX]
end

function game:forEachCell(func)
    for i = 1,CELLS_HORIZONTAL do
        for j = 1,CELLS_VERTICAL do
            func(i, j, self:getCell(i, j))
        end
    end
end

function game:mousereleased(x, y, button)
    local gridX = math.floor((x - BOARD_X) / CELL_SIZE) + 1
    local gridY = math.floor((y - BOARD_Y) / CELL_SIZE) + 1

    if button == 'l' then
        if gridX > 0 and gridX <= CELLS_HORIZONTAL and gridY > 0 and gridY <= CELLS_VERTICAL then
            local cell = self:getCell(gridX, gridY)
            if cell.state == 0 and cell.placeable and self.totalPlacedThisTurn < MAXIMUM_TILES_PER_TURN then
                cell.previousState = 0
                cell.state = 1
                self.totalPlayer = self.totalPlayer + 1
                self.totalPlacedThisTurn = self.totalPlacedThisTurn + 1
                cell.placedThisTurn = true
                cell:animate(ANIMATE_LENGTH)
                soundeffects.placeCell:rewind()
                soundeffects.placeCell:play()
            elseif cell.state == 1 and cell.placedThisTurn then
                cell.previousState = 1
                cell.state = 0
                self.totalPlayer = self.totalPlayer - 1

                if cell.placedThisTurn then
                    self.totalPlacedThisTurn = self.totalPlacedThisTurn - 1
                end

                cell:animate(ANIMATE_LENGTH)
                soundeffects.placeCell:rewind()
                soundeffects.placeCell:play()
            else
                soundeffects.cannotPlaceCell:rewind()
                soundeffects.cannotPlaceCell:play()
            end
        end
    end

    if debug and button == 'r' then
        local cell = self:getCell(gridX, gridY)
        if cell.state == 0 then
            cell.previousState = cell.state
            cell.state = 2
        else
            cell.previousState = cell.state
            cell.state = 0
        end

        cell:animate(ANIMATE_LENGTH)
    end

    if button == 'm' then
        local cell = self:getCell(gridX, gridY)
        print(cell.previousState, cell.state, cell.nextState, cell.size, gridX, gridY)

        if debug then
            cell.placeable = not cell.placeable
        end
    end
end

function game:update(dt)
    self.timer.update(dt)
    if self.currentState == state.simulating then
        self.timeSinceLastUpdate = self.timeSinceLastUpdate + math.min(dt, 0.025)

        if self.timeSinceLastUpdate > SIMULATION_STEP then
            self:simulateStep()
            self.timeSinceLastUpdate = self.timeSinceLastUpdate - SIMULATION_STEP

            self:checkWin()
        end
    end
end

function game:keypressed(key, isRepeat)
    if not isRepeat then
        if key == ' ' then
            self.currentState = (self.currentState == state.paused and state.simulating or state.paused)
            self.timeSinceLastUpdate = 0
        end

        if key == 'right' and self.currentState == state.paused then
            self:simulateStep()
            self:checkWin()
        end

        if key == 'r' then
            self:enter()
        end

        if key == 'return' and debug then
            print "return {"
            self:forEachCell(function(x, y, cell)
                print("[" .. x + (y - 1) * CELLS_HORIZONTAL .. "] = " .. (cell.placeable and 3 or cell.state) .. ",")
            end)

            print "}"
        end

        if key == "f1" then
            GameState.push(gamestates.help)
        end
    end
end

function game:checkWin()
    if self.health <= 0 then
        self.currentState = state.paused
        GameState.switch(gamestates.win, self.generation)
    end
end

function game:simulateStep(onlyUpdate)
    if not onlyUpdate then
        self:forEachCell(function(x, y, cell)
            local total = 0
            local colours = 0

            for i = -1,1 do
                for j = -1,1 do
                    local neighbour = self:getCell(x + i, y + j)
                    if neighbour.state == 1 then
                        total = total + 1
                        colours = colours + 1
                    elseif neighbour.state == 2 then
                        total = total + 1
                        colours = colours - 1
                    end
                end
            end

            if total == 3 then
                if cell.state == 0 then
                    cell:setNextState((colours > 0 and 1 or 2))
                else
                    cell:setNextState(cell.state)
                end
            elseif total == 4 then
                cell:setNextState(cell.state)
            else
                cell:setNextState(0)
            end
        end)

        soundeffects.nextGeneration:rewind()
        soundeffects.nextGeneration:play()
    end

    self.totalPlayer = 0
    self.totalOpponent = 0
    self.totalPlacedThisTurn = 0

    if self.generation % self.magicNumber < 1 then
        self:addStableShape()
    end

    self:forEachCell(function(x, y, cell)
        cell:generation()

        cell:animate(ANIMATE_LENGTH)

        if cell.state == 1 then
            self.totalPlayer = self.totalPlayer + 1
        elseif cell.state == 2 then
            self.totalOpponent = self.totalOpponent + 1
        end
    end)


    self.magicNumber = 10 * math.min(1, (self.totalOpponent + (debug and 0 or 1)) / (self.totalPlayer + 1))
    if not onlyUpdate then
        self.generation = self.generation + 1
        self.health = self.health + math.min(self.totalPlayer - self.totalOpponent, -1)
        if self.health < 0 then self.health = 0 end
    end
end

function game:draw()
    local oldCanvas = love.graphics.getCanvas()
    love.graphics.setCanvas(self.boardCanvas)
    love.graphics.clear()
    -- Squares for the cells
    self:forEachCell(function(x, y, cell)
        if cell.placeable then
            love.graphics.setColor(PLACEABLE_COLOUR)
            love.graphics.rectangle('fill', (x - 1) * CELL_SIZE, (y - 1) * CELL_SIZE, CELL_SIZE, CELL_SIZE)
        end

        if cell.state == 1 then
            love.graphics.setColor(cell.placedThisTurn and NEW_PLAYER_CELL_COLOUR or PLAYER_COLOUR)
        elseif cell.state == 2 then
            love.graphics.setColor(OPPONENT_COLOUR)
        elseif cell.previousState == 1 then
            love.graphics.setColor(cell.placedThisTurn and NEW_PLAYER_CELL_COLOUR or PLAYER_COLOUR)
        elseif cell.previousState == 2 then
            love.graphics.setColor(OPPONENT_COLOUR)
        end

        if cell.state ~= 0 or cell.previousState ~= 0 then
            love.graphics.rectangle('fill', (x - 0.5 - cell.size / 2) * CELL_SIZE, (y - 0.5 - cell.size / 2) * CELL_SIZE - cell.size / 2, CELL_SIZE * cell.size, CELL_SIZE * cell.size)
        end
    end)

    -- Lines
    love.graphics.setColor(170,170,170)
    for x = 1,CELLS_HORIZONTAL + 1 do
        love.graphics.line((x - 1) * CELL_SIZE, 0, (x - 1) * CELL_SIZE, CELLS_VERTICAL * CELL_SIZE)
    end
    for y = 1,CELLS_VERTICAL + 1 do
        love.graphics.line(0, (y - 1) * CELL_SIZE, CELLS_HORIZONTAL * CELL_SIZE, (y - 1) * CELL_SIZE)
    end
    love.graphics.setCanvas(oldCanvas)
    love.graphics.draw(backgroundImage, 0, 0)
    local oldShader = love.graphics.getShader()
    love.graphics.setShader(self.boardShader)
    self.boardShader:send("time", love.timer.getTime())
    love.graphics.draw(self.boardCanvas, BOARD_X, BOARD_Y)
    love.graphics.setShader(oldShader)

    -- Player Counter
    love.graphics.setFont(font)
    love.graphics.setColor(PLAYER_COLOUR)
    love.graphics.rectangle('fill', CELL_SIZE, CELL_SIZE, CELL_SIZE, CELL_SIZE)

    love.graphics.setColor(255, 255, 255)

    printWithCentre(tostring(self.totalPlayer), CELL_SIZE * 1.5, CELL_SIZE * 1.5)

    -- Opponent Counter
    love.graphics.setFont(font)
    love.graphics.setColor(OPPONENT_COLOUR)
    love.graphics.rectangle('fill', love.graphics.getWidth() - CELL_SIZE * 2, CELL_SIZE, CELL_SIZE, CELL_SIZE)

    love.graphics.setColor(255, 255, 255)
    printWithCentre(tostring(self.totalOpponent), love.graphics.getWidth() - CELL_SIZE * 1.5, CELL_SIZE * 1.5)

    love.graphics.setColor(255, 255, 255)
    if self.currentState == state.paused then
        printWithCentre("PAUSED", love.graphics.getWidth() / 2, CELL_SIZE)
    else
        printWithCentre("SIMULATING", love.graphics.getWidth() / 2, CELL_SIZE)
    end

    -- Generation counter
    printWithCentre("GENERATION: " .. self.generation, love.graphics.getWidth() / 2, CELL_SIZE * 2, fontSmall)

    -- Health
    love.graphics.setFont(font)
    love.graphics.print("HEALTH: " .. self.health, love.graphics.getWidth() - CELL_SIZE * 3 - font:getWidth("HEALTH: 1000"), CELL_SIZE)

    -- Tiles left to place
    love.graphics.setColor(PLAYER_COLOUR)
    for i = 1,(MAXIMUM_TILES_PER_TURN - self.totalPlacedThisTurn) do
        love.graphics.rectangle('fill', 0.5 * CELL_SIZE * i + CELL_SIZE * 4, CELL_SIZE * 1.25 + 1, 0.5 * CELL_SIZE - 2, 0.5 * CELL_SIZE - 2)
    end
end

return game

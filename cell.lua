local class = require "libs.hump.class"

local Cell = class{
    init = function(self, state, timer)
        if state == 3 then
            self.state = 0
            self.placeable = true
        else
            self.state = state
            self.placeable = (state == 1)
        end

        self.nextState = nil
        self.previousState = 0
        self.size = 0
        self.timer = timer
        self.placedThisTurn = false
    end
}

function Cell:setNextState(state)
    self.nextState = state
    if state == 1 then
        self.placeable = true
    end
end

function Cell:animate(animateLength)
    if self.previousState == 0 and self.state ~= 0 then
        self.timer.tween(animateLength, self, {size = 1}, 'linear')
    elseif self.previousState ~= 0 and self.state == 0 then
        self.timer.tween(animateLength, self, {size = 0}, 'linear')
    end

    if self.previousState == self.state and self.state ~= 0 then
        self.size = 1
    end
end

function Cell:generation()
    if self.nextState then
        self.previousState = self.state
        self.state = self.nextState
        self.nextState = nil

        self.placedThisTurn = false
    end
end

return Cell

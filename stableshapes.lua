local function stableShape(w, h, weight, data)
    return {width = w, height = h, weight = weight, data = data}
end

local shapes = {
    -- square
    stableShape(2, 2, 1,
        {1,1,
         1,1}),

    -- blinker
    stableShape(3, 1, 0.5,
        {1,1,1}),

    -- blinker
    stableShape(1, 3, 0.5,
        {1,
         1,
         1}),

    -- toad
    stableShape(4, 2, 0.5,
        {1,1,1,0,
         0,1,1,1}),

    -- toad
    stableShape(2, 4, 0.5,
        {1,0,
         1,1,
         1,1,
         0,1}),

    -- beacon
    stableShape(4, 4, 0.5,
        {1,1,0,0,
         1,1,0,0,
         0,0,1,1,
         0,0,1,1}),

    -- beacon
    stableShape(4, 4, 0.5,
        {0,0,1,1,
         0,0,1,1,
         1,1,0,0,
         1,1,0,0}),

    -- beehive
    stableShape(4, 3, 0.5,
        {0,1,1,0,
         1,0,0,1,
         0,1,1,0}),

    -- beehive
    stableShape(3, 4, 0.5,
        {0,1,0,
         1,0,1,
         1,0,1,
         0,1,0}),

    -- glider (to ruin everything)
    stableShape(3, 3, 0.25,
        {1,0,0,
         1,0,1,
         1,1,0}),

    -- glider (to ruin everything)
    stableShape(3, 3, 0.25,
        {0,0,1,
         1,0,1,
         0,1,1}),

    -- glider (to ruin everything)
    stableShape(3, 3, 0.25,
        {1,1,1,
         0,0,1,
         0,1,0}),

    -- glider (to ruin everything)
    stableShape(3, 3, 0.25,
        {1,1,1,
         1,0,0,
         0,1,0}),

    -- T-shape (my favourite for exploding)
    stableShape(3, 2, 0.1,
        {1,1,1,
         0,1,0}),

    stableShape(2, 3, 0.1,
        {1,0,
         1,1,
         1,0}),
}

local ret = {}

for _, shape in ipairs(shapes) do
    ret[shape] = shape.weight
end

return ret

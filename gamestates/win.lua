local Timer = require "libs.hump.timer"
local socket = require "socket"
local msgpack = require "libs.MessagePack"
local lume = require "libs.lume"

local level = require "level"

local win = {}

local width = math.max(CELL_SIZE * CELLS_HORIZONTAL, 500)
local height = 300
local x = BOARD_X
local y = BOARD_Y

playerName = ""

function win:enter(previous, score)
    self.score = score
    self.name = playerName
    self.acceptingInput = true

    self.previous = previous
    self.rectangle = {x, -height, width, height}

    self.highscoreData = nil
    self.errorMessage = nil

    self.timer = Timer.new()
    self.done = false

    self.gameCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())

    self.backgroundQuad = love.graphics.newQuad(0, 0, width, height, whiteBackground:getDimensions())

    self.greyscaleAmount = 0

    self.timer.tween(1, self, {greyscaleAmount = 1}, 'linear', function()
        self.timer.tween(1, self.rectangle, {x, y, width, height}, 'out-back')
        soundeffects.scrollDown:play()
    end)
end

function win:update(dt)
    self.timer.update(dt)
end

function win:keypressed(key, isRepeat)
    if key == 'backspace' and self.acceptingInput == true then
        if #self.name > 0 then
            soundeffects.keypress:play()
            self.name = string.sub(self.name, 1, #self.name - 1)
        end
    end

    if key == 'return' and self.acceptingInput == true and #self.name > 0 then
        soundeffects.keypress:play()
        self.acceptingInput = false
        playerName = self.name

        local client = socket.tcp()
        client:connect("ld31.gwilym.ninja", 8001)
        local message = msgpack.pack{name = self.name, score = self.score}
        client:send(tostring(#message) .. '\n' .. message)
        local ret = client:receive("*a")

        local ok, data = pcall(msgpack.unpack, ret)
        self.highscoreData = data
        if not ok then
            print("Failed to get highscore data")
            self.highscoreData = nil
            self.errorMessage = "Failed to get highscore data"
        else -- need to sort the high scores
            table.sort(self.highscoreData, function(l, r)
                return l.score < r.score
            end)

            local numberBetter = 0
            for i = 1,#self.highscoreData do
                if self.highscoreData[i].score > self.score then
                    numberBetter = numberBetter + 1
                end
            end

            self.highscoreData.position = self.highscoreData.position - numberBetter
        end
    elseif key == 'return' and self.acceptingInput == false and self.done == false then
        self.done = true
        soundeffects.scrollUp:play()
        self.timer.tween(1, self.rectangle, {x, -height, width, height}, 'sine', function()
            self.timer.tween(1, self, {greyscaleAmount = 0}, 'linear', function()
                GameState.switch(gamestates.game)
            end)
        end)
    end
end

function win:textinput(text)
    if self.acceptingInput and #self.name < 10 then
        soundeffects.keypress:play()
        self.name = self.name .. text
    end
end

function win:draw()
    local r = self.rectangle

    oldCanvas = love.graphics.getCanvas()
    love.graphics.setCanvas(self.gameCanvas)
    self.previous:draw()
    love.graphics.setCanvas(oldCanvas)

    love.graphics.setShader(greyscaleShader)
    greyscaleShader:send("grey", self.greyscaleAmount)
    love.graphics.draw(self.gameCanvas, 0, 0)
    love.graphics.setShader()

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(whiteBackground, self.backgroundQuad, r[1], r[2])

    love.graphics.setColor(0, 0, 0)
    love.graphics.setFont(fontSmall)

    love.graphics.print("GAME OVER", r[1] + 15, r[2] + 15)

    love.graphics.print("Score: ", r[1] + 15, r[2] + 50)
    love.graphics.print("Name: ", r[1] + 15, r[2] + 35)

    love.graphics.print(tostring(self.score), r[1] + 150, r[2] + 50)
    love.graphics.print(self.name .. (self.acceptingInput and '*' or ''),  r[1] +  150, r[2] + 35)

    if self.highscoreData then
        local total = #self.highscoreData
        for i, v in ipairs(self.highscoreData) do
            love.graphics.setColor(10,10,10)
            love.graphics.rectangle('fill', r[1] + width / 2, r[2] + (total - i) * height / 10 + 3, width / 2 - 3, height / 10 - 6)
            love.graphics.setColor(255, 255, 255)
            love.graphics.print(string.format("%i. %s - %i", total - i + 1 + self.highscoreData.position, v.name, v.score),
                r[1] + width / 2 + 10, r[2] + (total - i) * height / 10 + 5)
        end
    end

    if self.errorMessage then
        love.graphics.setColor(255, 0, 0)
        printWithCentre(self.errorMessage, r[1] + r[3] / 2, r[2] + r[4] / 2)
    end

    if self.highscoreData or self.errorMessage then
        love.graphics.setFont(fontSmall)
        love.graphics.setColor(0, 0, 0)
        love.graphics.print("PRESS ENTER TO RESTART", r[1] + 10, r[2] + r[4] - CELL_SIZE * 1.25)
    end
end

return win

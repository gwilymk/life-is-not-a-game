local sqlite3 = require("luasql.sqlite3").sqlite3()
local socket = require"socket"
local msgpack = require"libs.MessagePack"

local db = sqlite3:connect("highscores.db")

local function rows(cur)
    local row = cur:fetch({}, "a")
    local total = 0
    while row do
        total = total + 1
        row = cur:fetch(row, "a")
    end
    return total
end

local createCode = [[
CREATE TABLE highscores (name TEXT, score INTEGER, address TEXT, time INTEGER);
]]

local newEntry = [[
INSERT INTO highscores VALUES ('%s', %i, '%s', %i);
]]

local getEntries = [[
SELECT name, score FROM highscores ORDER BY ABS(score - %i) LIMIT 10;
]]

local numEntries = [[
SELECT COUNT(*) FROM highscores WHERE score > %i;
]]

db:execute(createCode)

local server = socket.tcp()
local shutdown

do
    local devuradom = io.open("/dev/urandom", "r")
    shutdown = devuradom:read(20)
    devuradom:close()

    local file = io.open("random.txt", "w")
    file:write(shutdown)
    file:close()
end

server:bind('*', 8001)
server:listen(5)

while true do
    local client = server:accept()
    print("Got connection from " .. client:getpeername())

    local length = client:receive("*l")
    if length == shutdown then
        client:close()
        break
    end

    local data
    if tonumber(length) then
        data = client:receive(tonumber(length))
    end

    local ok, unpacked = pcall(msgpack.unpack,tostring(data))

    if ok and unpacked and unpacked.name and tonumber(unpacked.score) then
        if #tostring(unpacked.name) < 11 and tonumber(unpacked.score) > 0 then
            local statement = string.format(newEntry, string.gsub(string.gsub(unpacked.name, "\\", "\\\\"), "'", "\\'"), tonumber(unpacked.score), client:getpeername(), os.time())
            assert(db:execute(statement))
            local cur = assert(db:execute(string.format(getEntries, unpacked.score, unpacked.score)))

            local retData = {}
            local row = cur:fetch({}, "a")
            while row do
                table.insert(retData, {name = row.name, score = row.score})
                row = cur:fetch(row, "a")
            end

            retData.position = assert(db:execute(string.format(numEntries, unpacked.score))):fetch({})[1]
            client:send(msgpack.pack(retData))
            cur:close()
        end
    end

    print("Closing connection")
    client:close()
end

db:close()
server:close()

local Timer = require "libs.hump.timer"

local help = {}

local helpText = [[
1.  Any live cell with fewer than two live neighbours dies, as if caused by under-population.
2.  Any live cell with two or three live neighbours lives on to the next generation.
3.  Any live cell with more than three live neighbours dies, as if by overcrowding.
4.  Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction. The
    colour of the new cell is whichever colour had the majority of the three required to produce it.
5.  You may only place your orange cells on the purple sections. Place by left clicking.
6.  New purple sections are created as your cells expand.
7.  Press space to toggle the simulation.
8.  Press right to move one step in the simulation.
9.  r restarts the game.
10. The game ends when you have no health remaining.
11. Your score is the number of generations you survived for.
12. Health goes down faster if you have fewer cells then the opponent.
13. You may only place 5 cells per generation.
14. During any given generation, you may only remove tiles you placed during that generation.
]]

local x, y, width, height

width = fontSmall:getWidth(helpText) + 16
height = 16 * fontSmall:getHeight()
x = (love.graphics.getWidth() - width) / 2
y = (love.graphics.getHeight() - height) / 2

function help:enter(previous)
    self.previous = previous
    self.timer = Timer.new()

    self.rectangle = {x, -height, width, height}
    self.timer.tween(1, self.rectangle, {x, y, width, height}, 'out-back')

    soundeffects.scrollDown:play()
    self.acceptingInput = true

    self.backgroundQuad = love.graphics.newQuad(0, 0, width, height, whiteBackground:getDimensions())
end

function help:update(dt)
    self.timer.update(dt)
end

function help:keypressed()
    self.acceptingInput = false
    self:close()
end

function help:mousereleased()
    self.acceptingInput = false
    self:close()
end

function help:draw()
    self.previous:draw()

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(whiteBackground, self.backgroundQuad, self.rectangle[1], self.rectangle[2])

    love.graphics.setColor(0, 0, 0)
    love.graphics.setFont(fontSmall)
    love.graphics.print(helpText, self.rectangle[1] + 8, self.rectangle[2] + fontSmall:getHeight() / 2)
end

function help:close()
    soundeffects.scrollUp:play()
    self.timer.tween(1, self.rectangle, {x, -height, width, height}, 'out-sine', function()
        GameState.pop()
    end)
end
return help

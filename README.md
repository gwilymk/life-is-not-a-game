Life is not a game
==================

A game about life, survival and high scores. My entry for [LD31](http://ludumdare.com/compo/ludum-dare-31/?action=preview&uid=2430)

About
-----

You'll need [love2d](http://love2d.org) in order to play this. Read the manual, it'll help.

Thanks
------

The music was made with abundant-music.org, and without that site, this game would have no music as
I have no composing talent. I changed pretty much all of the instruments, removed a track and took
out a verse, but all in all, it was nice to have music with this entry without the need to compose
it myself.

Love2d, and the libraries I ended up using (hump and lume) made the game development itself much easier.

The wikipedia article about Conway's game of life.

And all the software I used to put this together I haven't mentioned (its a lot).

Download
--------

[Linux/Mac/Windows](http://goo.gl/DTq0KN)

[Manual](http://goo.gl/X5tEvV)

Licence
-------

All code copyright &copy; Gwilym Kuiper (gw@ilym.me) 2014 all code is under GPL version 3.0.

All graphics, music and sound copyright &copy; Gwilym Kuiper (gw@ilym.me) 2014 and is released under
Creative Commons Attribution-ShareAlike 4.0 International (https://creativecommons.org/licenses/by-sa/4.0/)

The font is released under the open font licence.

Contact
-------

If you want to get in touch, please email me at lifeisnotagame@gwilym.ninja and I'll do my
best to reply.

Bugs
----

Either fill a bug report in with the bitbucket issue tracker or send me an email.

local socket = require "socket"

local s = socket.tcp()

s:connect("localhost", 8001)

local f = io.open("random.txt", "r")
s:send(f:read("*a") .. "\n")

s:close()
f:close()

extern number grey;

vec4 effect(vec4 colour, Image texture, vec2 tc, vec2 sc) {
    vec4 imageColour = Texel(texture, tc);

    number greyColour = 0;
    greyColour += imageColour.r * imageColour.r;
    greyColour += imageColour.g * imageColour.g;
    greyColour += imageColour.b * imageColour.b;

    greyColour = sqrt(greyColour / 3);

    vec4 greyC = vec4(greyColour, greyColour, greyColour, 1);

    return mix(imageColour, greyC, grey);
}
